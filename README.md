## Wyam blog
My Blog Site written in wyam published to note.iqubit.xyz (deprecated)

### Bootstrapper Config
In past, around 2020, bootstrap was slightly different,

    .CreateDefaultWithout(args, DefaultFeatures.Pipelines)

instead of following at present,

    .CreateWeb(args)


### wyam deploy yml
Retained from from file history around 2020,

    # Starter pipeline
    # Start with a minimal pipeline that you can customize to build and deploy your code.
    # Add steps that build, run tests, deploy, and more:
    # https://aka.ms/yaml

    trigger:
    branches:
        include:
        - source
        exclude:
        - master
    paths:
        exclude:
        - .gitignore
        - README.md
    pool:
    vmImage: 'windows-2019'

    steps:
    - task: cake-build.cake.cake-build-task.Cake@0
        displayName: "Build and publish Wyam site"
        inputs:
        target: Deploy
        env:
        GITHUB_REPO_URL: $(githubUserRepoUrl)
        GITHUB_ACCESS_TOKEN: $(githubAccessToken)
        GITHUB_USERNAME: "$(githubUserName)"
        GITHUB_USEREMAIL: "$(githubUserEmail)"
        CNAME_CONTENT: "$(CNameContent)"
